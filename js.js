// ## Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.
//1. функція, яка може викидати винятки
//2. робота з асинхронним кодом
//3. коли ми взаємодіємо з серверами чи бд
//4. коли ми тестуємо коди і шукаємо помилки
// ## Завдання
// Дано масив books.

// ```javascript
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];
// ```

// - Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// - На сторінці повинен знаходитись `div` з `id="root"`, куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// - Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
// Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// - Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

let divus = document.createElement("div");
divus.setAttribute("id", "root");
let body = document.querySelector(".body");

let ul = document.createElement("ul");

body.append(divus);
divus.append(ul);

let mass = [`author`, `name`, `price`];
let fram = document.createDocumentFragment();

validate(books, mass);

function validate(books, mass) {
  let arr2 = mass.toString();
  books.forEach((element) => {
    try {
      let arr1 = Object.keys(element).toString();
      if (arr1 === arr2) {
        render(element);
      } else {
        error(element);
      }
    } catch (error) {
      console.error(error.message);
    }
  });
}

function error(item) {
  let keyArray = {};
  books.forEach((el) => {
    keyArray = { ...keyArray, ...el };
  });

  const filterArray = [];

  Object.keys(keyArray).find((value) => {
    if (item[value] === undefined) {
      filterArray.push(value);
    }
  });

  if (filterArray.length > 0) {
    throw new Error(`Не вистачає властивості: ${filterArray.join(", ")}`);
  }
}
function render(element) {
  const bookLi = document.createElement("li");
  const bookAuthor = document.createElement("p");
  bookAuthor.textContent = `Author: ${element.author}`;
  const bookName = document.createElement("p");
  bookName.textContent = `Name: ${element.name}`;
  const bookPrice = document.createElement("p");
  bookPrice.textContent = `Price: ${element.price}`;
  bookLi.append(bookAuthor, bookName, bookPrice);
  fram.append(bookLi);
  ul.append(fram);
}
